export interface Contacts{
    contactId : string;
    lastname : string;
    firstname: string;
    email : string;
    dateOfJoin: Date;
    inProject: boolean;
    address : Address;
    office : string;
}

export interface Address {
    steet: string;
    houseNo: string;
    postCode: number;
    city: string;
    country: string;
}