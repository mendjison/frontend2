import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import * as model from '../model/index';

@Injectable({providedIn:"root"})
export class ContactsService {

    baseUrl: string = environment.server;

    constructor(private httpClient: HttpClient) {}

    public findContacts(): Observable<model.Contacts[]> {
        return this.httpClient.get<model.Contacts[]>(`${this.baseUrl}/contacts`);
    }

    public findContactById(contactId: string): Observable<model.Contacts> {
        return this.httpClient.get<model.Contacts>(`${this.baseUrl}/contacts/${contactId}`);
    }
    
    public findContactsInProject(inProject:boolean): Observable<model.Contacts[]> {
        return this.httpClient.get<model.Contacts[]>(`${this.baseUrl}/contacts?inProject=${inProject}`);
    }

    public findContactsByDateOfJoinin(dateOfJoinin:Date): Observable<model.Contacts[]> {
        return this.httpClient.get<model.Contacts[]>(`${this.baseUrl}/contacts?inProject=${dateOfJoinin}`);
    }

    public findContactsByOffice(office:string): Observable<model.Contacts[]> {
        return this.httpClient.get<model.Contacts[]>(`${this.baseUrl}/contacts?inProject=${office}`);
    }

}