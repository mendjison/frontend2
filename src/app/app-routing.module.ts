import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddContactComponent } from './components/contacts/add-contact/add-contact.component';
import { EditContactComponent } from './components/contacts/edit-contact/edit-contact.component';
import {ContactsComponent } from './components/contacts/contacts.component';

const routes: Routes = [
  {path:'contacts/add', component: AddContactComponent},
  {path: 'contacts/edit/:contactId', component: EditContactComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '', redirectTo: 'contacts', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
