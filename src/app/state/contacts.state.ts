import * as model from 'src/app/backend/model/index';

export enum StateStatus {
    LOADED="Loaded",
    LOADING="Loading",
    IINTIAL="Initial"
}

export interface ContactState {
    status: StateStatus;
    data: model.Contacts[],
    errorMessage: string;
}

/* export interface ContactState<T> {
    status: StateStatus;
    data: T[],
    errorMessage: string;
} */