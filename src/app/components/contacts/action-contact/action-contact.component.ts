import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-action-contact',
  templateUrl: './action-contact.component.html',
  styleUrls: ['./action-contact.component.css']
})
export class ActionContactComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onGetContacts() {}

  onGetContactsInProject() {}
  
  onSearch(searchValue: string){}
}
