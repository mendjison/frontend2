import * as model from 'src/app/backend/models/index';

export interface IGender {
    value: model.GenderType,
    viewValue: string
  }
  
  export interface IPhone {
    value: model.PhoneType,
    viewValue: string
  }
  
  export interface IRelationType {
    value: model.RelationShipType,
    viewValue: string
  }