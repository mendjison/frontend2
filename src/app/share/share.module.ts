import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SprinnersComponent } from './sprinners/sprinners-component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


const shareModule = [CommonModule,
  HttpClientModule,
    FormsModule,
    ReactiveFormsModule
]

@NgModule({
  declarations: [SprinnersComponent],
  imports: [
    ...shareModule
  ],
  providers: [],
  exports: [SprinnersComponent, ...shareModule]
})
export class ShareModule { }
