import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShareModule } from './share/share.module';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { AddContactComponent } from './components/contacts/add-contact/add-contact.component';
import { EditContactComponent } from './components/contacts/edit-contact/edit-contact.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ActionContactComponent } from './components/contacts/action-contact/action-contact.component';
import { ListContactsComponent } from './components/contacts/list-contacts/list-contacts.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//import { ContactComponent } from './components/contacts/list-contacts/contact';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AddContactComponent,
    EditContactComponent,
    ContactsComponent,
    ActionContactComponent,
    ListContactsComponent,
    
    //ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
    //ShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
